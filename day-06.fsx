let directOrbits =
  System.IO.File.ReadAllLines "day-06-input.txt"
  |> Seq.map ((fun x -> x.Split ")") >> (fun x -> x.[1], x.[0]))
  |> Map

let rec allOrbits obj =
  match directOrbits |> Map.tryFind obj with
  | Some x -> x :: allOrbits x
  | None -> []

printfn "Part 1: %d" (directOrbits |> Map.toSeq |> Seq.map fst |> Seq.sumBy (allOrbits >> List.length))

let santaOrbits = allOrbits "SAN"
let rec transferCount cnt obj =
  match santaOrbits |> List.tryFindIndex ((=) obj) with
  | None -> transferCount (cnt + 1) directOrbits.[obj]
  | Some idx -> cnt + idx - 1

printfn "Part 2: %d" (transferCount 0 "YOU")
