open System.Collections.Generic

let input = System.IO.File.ReadAllLines "day-18-input.txt"

type NodeType = Open | Player | Key | Door
type Node = { ID: int; Type: NodeType; Mask: int; mutable Edges: (Node * int) list }

let fullCharMap =
  input
  |> Seq.indexed
  |> Seq.collect (fun (y, s) -> s |> Seq.indexed |> Seq.map (fun (x, c) -> ((x, y), c)))
  |> Map

let letterToId ch = ch |> System.Char.ToUpper |> int |> fun x -> x - int 'A'
let letterToMask ch = 1 <<< letterToId ch
let charToNode x y ch =
  let node nodeType = { ID = 0; Type = nodeType; Mask = 0; Edges = [] }
  match ch with
  | '.' -> Some <| node Open
  | '@' -> Some <| node Player
  | x when x >= 'A' && x <= 'Z' -> x |> fun x -> Some { node Door with ID = letterToId x; Mask = letterToMask x }
  | x when x >= 'a' && x <= 'z' -> x |> fun x -> Some { node Key with ID = 50 + letterToId x; Mask = letterToMask x }
  | _ -> None

let charMapToNodeMap map =
  map
  |> Map.map (fun (x, y) c -> charToNode x y c)
  |> Map.filter (fun _ x -> Option.isSome x)
  |> Map.map (fun _ x -> Option.get x)

let neighbors (x, y) = seq { x - 1, y; x + 1, y; x, y - 1; x, y + 1 }

let findEdges (pos: int * int) (nodeMap: Map<int * int, Node>) =
  let shortestPaths = new Dictionary<int * int, int>()
  let shortestPath pos =
    let mutable x = 0
    if shortestPaths.TryGetValue(pos, &x) then Some x else None
  let rec explore pos steps =
    match nodeMap |> Map.tryFind pos, shortestPath pos with
    | None, _ -> Seq.empty // out of bounds
    | _, Some x when steps >= x -> Seq.empty // we already have a quicker way to get here
    | Some node, _ ->
      shortestPaths.[pos] <- steps
      if steps <> 0 && (node.Type = Door || node.Type = Key) then
        seq { pos } // the node is a door or key - stop here
      else
        neighbors pos |> Seq.collect (fun x -> explore x (steps + 1))
  explore pos 0
    |> Seq.map (fun x -> nodeMap.[x], shortestPath x |> Option.get)
    |> Seq.toList

// Build a proper graph with edges. Open nodes will be removed. Walls and keys will be connected to all reachable
// walls/keys. The player node will be connected to all reachable walls/keys and will be returned.
let buildGraph (nodeMap: Map<int * int, Node>) =
  nodeMap |> Map.iter (fun pos node -> if node.Type <> Open then node.Edges <- nodeMap |> findEdges pos)
  nodeMap |> Map.toSeq |> Seq.map snd |> Seq.find (fun x -> x.Type = Player)

let shortest (targetMask: int) (player: Node): int =
  let cache = new Dictionary<int64, int>()
  let mutable best = System.Int32.MaxValue

  let unexplored = new Stack<int * int * Node>()
  unexplored.Push(0, 0, player)
  while unexplored.Count > 0 do
    let cur, heldKeys, node = unexplored.Pop()
    let mutable prev = 0
    if cur >= best then
      () // no need to proceed further down this path
    else
      let hash = (int64 node.ID <<< 32) ||| int64 heldKeys
      if cache.TryGetValue(hash, &prev) && cur >= prev then
        () // no need to proceed further down this path
      else
        cache.[hash] <- cur
        if heldKeys = targetMask then
          if cur < best then
            best <- cur
        else
          node.Edges
            |> Seq.filter (fun (x, _) -> x.Type = Door && x.Mask &&& heldKeys <> 0)
            |> Seq.iter (fun (x, w) -> unexplored.Push(cur + w, heldKeys, x))
          node.Edges
            |> Seq.filter (fun (x, _) -> x.Type = Key)
            |> Seq.iter (fun (x, w) -> unexplored.Push(cur + w, heldKeys ||| x.Mask, x))

  best

let fullNodeMap = fullCharMap |> charMapToNodeMap
let allKeys = fullNodeMap |> Map.toSeq |> Seq.map (fun (_, x) -> x.Mask) |> Seq.reduce (|||)
printfn "Part 1: %d" (fullNodeMap |> buildGraph |> shortest allKeys)

let subGraph region playerPos =
  fullCharMap
  |> Map.toSeq
  |> Seq.filter (fst >> region)
  |> Map
  |> Map.add playerPos '@'
  |> charMapToNodeMap
  |> buildGraph

let shortest4 (targetMask: int) (p1: Node, p2: Node, p3: Node, p4: Node): int =
  let cache = new Dictionary<int64, int>()
  let mutable best = System.Int32.MaxValue

  let unexplored = new Stack<int * int * (Node * Node * Node * Node)>()
  unexplored.Push(0, 0, (p1, p2, p3, p4))
  while unexplored.Count > 0 do
    let cur, heldKeys, (a, b, c, d) = unexplored.Pop()
    //printfn "TEMP: %X (%d %d %d %d)" heldKeys a.ID b.ID c.ID d.ID
    let mutable prev = 0
    if cur >= best then
      () // no need to proceed further down this path
    else
      let shift n by = (int64 n.ID) <<< by
      let hash = int64 heldKeys ||| shift a 32 ||| shift b 40 ||| shift c 48 ||| shift d 56
      if cache.TryGetValue(hash, &prev) && cur >= prev then
        () // no need to proceed further down this path
      else
        cache.[hash] <- cur
        if heldKeys = targetMask then
          if cur < best then
            best <- cur
        else
          let explore p merge =
            for e, w in p.Edges do
              if e.Type = Door && e.Mask &&& heldKeys <> 0 then
                unexplored.Push(cur + w, heldKeys, merge e)
              if e.Type = Key then
                unexplored.Push(cur + w, heldKeys ||| e.Mask, merge e)
          explore a (fun x -> x, b, c, d)
          explore b (fun x -> a, x, c, d)
          explore c (fun x -> a, b, x, d)
          explore d (fun x -> a, b, c, x)

  best

// Part 2: each sub-map is 40x40.
let subPlayers = (
  subGraph (fun (x, y) -> x < 40 && y < 40) (39, 39),
  subGraph (fun (x, y) -> x > 40 && y < 40) (41, 39),
  subGraph (fun (x, y) -> x < 40 && y > 40) (39, 41),
  subGraph (fun (x, y) -> x > 40 && y > 40) (41, 41))
printfn "Part 2: %d" (shortest4 allKeys subPlayers)
