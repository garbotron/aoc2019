let input = System.IO.File.ReadAllText "day-04-input.txt"
let min, max = input.Trim().Split '-' |> fun x -> x.[0], x.[1]

let decreases str = str |> Seq.pairwise |> Seq.exists (fun (x, y) -> x > y)
let hasPair str = str |> Seq.groupBy id |> Seq.map (snd >> Seq.length) |> Seq.exists ((>=) 2)
let hasPairStrict str = str |> Seq.groupBy id |> Seq.map (snd >> Seq.length) |> Seq.exists ((=) 2)

let legal part = function
  | x when x < min -> false
  | x when x > max -> false
  | x when decreases x -> false
  | x when part = 1 && not (hasPair x) -> false
  | x when part = 2 && not (hasPairStrict x) -> false
  | _ -> true

let allStrings = { 0 .. 999999 } |> Seq.map (sprintf "%06d")

let part n = printfn "Part %d: %d" n (allStrings |> Seq.filter (legal n) |> Seq.length)
[1; 2] |> List.map part
