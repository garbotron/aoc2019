open System.Text.RegularExpressions
open System.Numerics

type Deal = DealIntoNewStack | Cut of int64 | DealWithIncrement of int64

let parseDeal (str: string) =
  if str.StartsWith "deal with increment " then
    DealWithIncrement (str.Substring ("deal with increment ".Length) |> int64)
  elif str.StartsWith "cut " then
    Cut (str.Substring ("cut ".Length) |> int64)
  elif str = "deal into new stack" then
    DealIntoNewStack
  else
    raise <| System.Exception()

let input =
  System.IO.File.ReadAllLines "day-22-input.txt"
  |> Array.toList
  |> List.map parseDeal

// Euclidean remainder, the proper modulo operation.
let inline (%!) n m = ((n % m) + m) % m

// Given coefficients a and b, get the next iteration of a and b after the given deal is executed.
// a and b form the equation "m = (a n + b) mod L" where n is the cur index of a given card and m is the next index.
// This function took a long time to figure out (see the ramblings in day-22-notes.txt).
let nextCoefficients deckSize (a, b) = function
  | DealIntoNewStack -> deckSize - a, -b - 1L
  | Cut c -> a, b - c
  | DealWithIncrement c -> (c * a) %! deckSize, (c * b) %! deckSize

let applyCoefficients idx deckSize (a, b) = (a * idx + b) %! deckSize

printfn "Part 1: %d" (input |> List.fold (nextCoefficients 10007L) (1L, 0L) |> applyCoefficients 2019L 10007L)

// Calculate the a and b coefficients (in the forumla m = a n + b) to execute the input a given number of times.
// This uses recursion to break down / chain the applications. See the notes for rambling semi-explanations.
let rec calcCoefficientsForCycleCount (cycles: int64) (deckSize: bigint) (c1a: bigint, c1b: bigint): bigint * bigint =
  if cycles = 1L then
    c1a, c1b
  else
    let wrap x = x %! deckSize
    let a, b = calcCoefficientsForCycleCount (cycles / 2L) deckSize (c1a, c1b)
    let a' = a * a |> wrap
    let b' = (a * b) + b |> wrap
    if cycles % 2L = 0L then
      a', b'
    else
      let a'' = c1a * a' |> wrap
      let b'' = (c1a * b') + c1b |> wrap
      a'', b''

let calcCoefficientsForCycleCount64 (cycles: int64) (deckSize: int64) (c1a: int64, c1b: int64): int64 * int64 =
  let a, b = calcCoefficientsForCycleCount cycles (bigint deckSize) (bigint c1a, bigint c1b)
  int64 a, int64 b

let p2deck = 119315717514047L
let p2iters = 101741582076661L

// Calculate the a and b coefficients for a single full cycle of the input sequence.
let p2oneCycle = input |> List.fold (nextCoefficients p2deck) (1L, 0L)

// Calculate the a and b coefficients for the full number of iterations of the input sequence.
let p2a, p2b = calcCoefficientsForCycleCount64 p2iters p2deck p2oneCycle

// We now know a and b in "m = (a n + b) mod L". We want m = 2020, solve for n.
// Used Wolfram Alpha to calculate the last equivalency. Exact answer redacted as a spoiler.
