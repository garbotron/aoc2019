let input =
  System.IO.File.ReadAllLines "day-10-input.txt"
  |> Seq.indexed
  |> Seq.collect (fun (y, str) -> str |> Seq.indexed |> Seq.filter (snd >> ((=) '#')) |> Seq.map (fst >> fun x -> x, y))
  |> Set

let add (x1, y1) (x2, y2) = x1 + x2, y1 + y2
let sub (x2, y2) (x1, y1) = x1 - x2, y1 - y2
let mul (x, y) z = x * z, y * z
let ray (x, y) =
  let divisor = { 50 .. -1 .. 1 } |> Seq.find (fun i -> x % i = 0 && y % i = 0)
  x / divisor, y / divisor

let reposition station stars =
  stars |> Set.map (sub station) |> Set.remove (0, 0)

let canSee stars target =
  let hit = Seq.initInfinite (mul (ray target)) |> Seq.find (fun x -> stars |> Set.contains x)
  hit = target // check that we hit our target (instead of something else)

let countStars station =
  input |> reposition station |> fun x -> x |> Set.filter (canSee x) |> Set.count

let station, starCount = input |> Seq.map (fun x -> x, countStars x) |> Seq.maxBy snd
printfn "Part 1: %d" starCount

// returns (quadrant, slope) tuple where quadrant is int 0..3 and slope is float 0..1.
let angle = function
  | x, 0 -> if x < 0 then 3, 0.0 else 1, 0.0
  | 0, y -> if y < 0 then 0, 0.0 else 2, 0.0
  | x, y when x > 0 && y < 0 -> 0, float x / float -y
  | x, y when x > 0 && y > 0 -> 1, float y / float x
  | x, y when x < 0 && y > 0 -> 2, float -x / float y
  | x, y when x < 0 && y < 0 -> 3, float -y / float -x
  | _ -> raise <| System.Exception()

let rec vaporize count ang stars =
  let targets = stars |> fun x -> x |> Set.filter (canSee x) |> Seq.map (fun x -> x, angle x) |> Seq.sortBy snd
  let star, (quad, a) =
    targets |> Seq.filter (fun (_, x) -> x > ang) |> Seq.tryHead |> Option.defaultValue (Seq.head targets)
  if count = 1 then star
  else vaporize (count - 1) (quad, a + 0.00001) (stars |> Set.remove star)

vaporize 200 (0, -1.0) (input |> reposition station)
  |> add station
  |> fun (x, y) -> x * 100 + y
  |> printfn "Part 2: %d"
