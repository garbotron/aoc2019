//#region Intcode Computer (same as day 9)
type CpuState = Running | Halted | WaitingForInput
type Cpu = { State: CpuState; Mem: Map<int64, int64>; PC: int64; RelBase: int64; Input: int64 list; Output: int64 list }

let input = (System.IO.File.ReadAllText "day-19-input.txt").Split ","
let program = input |> Seq.indexed |> Seq.map (fun (x, y) -> int64 x, int64 y) |> Map
let initCpu = { State = Running; Mem = program; PC = 0L; RelBase = 0L; Input = []; Output = [] }

let rec run (cpu: Cpu): Cpu =
  let code = cpu.Mem.[cpu.PC] |> string |> fun x -> x.PadLeft(5, '0')

  let offset i =
    match code.[3 - i] with
    | '0' -> cpu.Mem.[cpu.PC + int64 i]
    | '1' -> cpu.PC + int64 i
    | '2' -> cpu.Mem.[cpu.PC + int64 i] + cpu.RelBase
    | _ -> raise <| System.Exception()

  let get i = cpu.Mem |> Map.tryFind (offset i) |> Option.defaultValue 0L
  let set i v = cpu.Mem |> Map.add (offset i) v

  match code.[3..4] with
  | "01" -> run { cpu with Mem = set 3 (get 1 + get 2); PC = cpu.PC + 4L }
  | "02" -> run { cpu with Mem = set 3 (get 1 * get 2); PC = cpu.PC + 4L }
  | "03" -> match cpu.Input with
            | [] -> { cpu with State = WaitingForInput }
            | x::y -> run { cpu with Mem = set 1 x; PC = cpu.PC + 2L; Input = y }
  | "04" -> run { cpu with Output = get 1 :: cpu.Output; PC = cpu.PC + 2L }
  | "05" -> run { cpu with PC = (if get 1 = 0L then cpu.PC + 3L else get 2) }
  | "06" -> run { cpu with PC = (if get 1 <> 0L then cpu.PC + 3L else get 2) }
  | "07" -> run { cpu with Mem = set 3 (if get 1 < get 2 then 1L else 0L); PC = cpu.PC + 4L }
  | "08" -> run { cpu with Mem = set 3 (if get 1 = get 2 then 1L else 0L); PC = cpu.PC + 4L }
  | "09" -> run { cpu with RelBase = cpu.RelBase + (get 1); PC = cpu.PC + 2L }
  | "99" -> { cpu with State = Halted }
  | _ -> raise <| System.Exception()
//#endregion

let isPulled (x, y) =
  { initCpu with Input = [int64 x; int64 y] }
  |> run
  |> fun c -> c.Output.Head = 1L

let boxCoords x y w h = seq {
  for x' in x .. x + w - 1 do
    for y' in y .. y + h - 1 do
      yield x', y'
}

printfn "Part 1: %d" (boxCoords 0 0 50 50 |> Seq.filter isPulled |> Seq.length)

// Note that we don't need to check if every coord is pulled, just the corners.
let fitsSquare (x, y) = [x, y; x + 99, y; x, y + 99; x + 99, y + 99] |> List.forall isPulled

let legalSquaresForY y =
  Seq.initInfinite (fun x -> x, y)
    |> Seq.skipWhile (not << isPulled)
    |> Seq.takeWhile isPulled
    |> Seq.filter fitsSquare

// Skip 5 because some of the first few Y stripes have holes (no Xs present).
let legalSquares = Seq.initInfinite id |> Seq.skip 5 |> Seq.collect legalSquaresForY

printfn "Part 2: %d" (legalSquares |> Seq.head |> fun (x, y) -> x * 10000 + y)
