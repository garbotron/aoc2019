open System.Collections.Generic

let input = (System.IO.File.ReadAllText "day-16-input.txt").Trim() |> Seq.toList |> List.map (string >> int)
let offset = input |> List.take 7 |> List.map string |> List.reduce (+) |> int

let inline elem (input: int list) (idx: int): int =
  let cycle = Seq.skip 1 <| seq {
    while true do
      for _ in { 0 .. idx } do yield 0
      for _ in { 0 .. idx } do yield 1
      for _ in { 0 .. idx } do yield 0
      for _ in { 0 .. idx } do yield -1
  }
  Seq.zip input cycle |> Seq.sumBy (fun (x, y) -> x * y) |> fun x -> abs x % 10

let inline phase (input: int list): int list =
  [ 0 .. input.Length - 1 ] |> List.map (fun i -> elem input i)

let ftt (phases: int) (input: int list): int list =
  [ 1 .. phases ] |> List.fold (fun x _ -> phase x) input

printfn "Part 1: %s" (input |> ftt 100 |> List.take 8 |> List.map string |> List.reduce (+))

let rec ftt' (phases: int) (input: int list): int list =
  if phases = 0 then input
  else
    let sums = List.scanBack (+) input 0
    let next = sums |> List.map (fun x -> abs x % 10)
    ftt' (phases - 1) next

let extInput = List.init (input.Length * 10000 - offset) (fun i -> input.[(offset + i) % input.Length])
printfn "Part 2: %s" (extInput |> ftt' 100 |> List.take 8 |> List.map string |> List.reduce (+))
