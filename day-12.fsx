open System.Collections.Generic;

type Moon = { Pos: int * int * int; Vel: int * int * int }

let parseXYZ (str: string) =
    let nums = str.Split ',' |> Array.map (fun x -> x.Trim('<', '>', ' ', 'x', 'y', 'z', '='))
    (int nums.[0], int nums.[1], int nums.[2])

let input =
  System.IO.File.ReadAllLines "day-12-input.txt"
  |> Seq.map parseXYZ
  |> Seq.toList
  |> List.map (fun p -> { Pos = p; Vel = 0, 0, 0 })

let energy moon =
  let coord (x, y, z) = abs(x) + abs(y) + abs(z)
  coord moon.Pos * coord moon.Vel

let rec run cycles moons =
  let add (x, y, z) (x', y', z') = x + x', y + y', z + z'
  let pull x x' = if x' > x then 1 elif x' < x then -1 else 0
  let gravity (x, y, z) (x', y', z') = pull x x', pull y y', pull z z'
  let dv moon = moons |> List.map (fun x -> gravity moon.Pos x.Pos) |> List.fold add (0, 0, 0)
  let updatedVel = moons |> List.map (fun x -> { x with Vel = add x.Vel (dv x) })
  let updated = updatedVel |> List.map (fun x -> { x with Pos = add x.Pos x.Vel })
  if cycles = 1 then updated else run (cycles - 1) updated

printfn "Part 1: %d" (input |> run 1000 |> List.sumBy energy)

let step (a, b, c, d, va, vb, vc, vd) =
  let pull x y = if y > x then 1 elif y < x then -1 else 0
  let va' = va + (pull a b) + (pull a c) + (pull a d)
  let vb' = vb + (pull b a) + (pull b c) + (pull b d)
  let vc' = vc + (pull c a) + (pull c b) + (pull c d)
  let vd' = vd + (pull d a) + (pull d b) + (pull d c)
  let a' = a + va'
  let b' = b + vb'
  let c' = c + vc'
  let d' = d + vd'
  Some ((a, b, c, d, va, vb, vc, vd), (a', b', c', d', va', vb', vc', vd'))

let cycleLength (a, b, c, d) =
  let init = (a, b, c, d, 0, 0, 0, 0)
  Seq.unfold step init |> Seq.indexed |> Seq.skip 1 |> Seq.find (snd >> ((=) init)) |> fst

let xs = input |> List.map (fun x -> x.Pos |> fun (x, _, _) -> x)
let ys = input |> List.map (fun x -> x.Pos |> fun (_, y, _) -> y)
let zs = input |> List.map (fun x -> x.Pos |> fun (_, _, z) -> z)
let xCycle = cycleLength (xs.[0], xs.[1], xs.[2], xs.[3])
let yCycle = cycleLength (ys.[0], ys.[1], ys.[2], ys.[3])
let zCycle = cycleLength (zs.[0], zs.[1], zs.[2], zs.[3])

let rec gcd x y = if y = 0L then x else gcd y (x % y)
let lcm x y = x * y / (gcd x y)

printfn "Part 2: %d" (lcm (int64 xCycle) (lcm (int64 yCycle) (int64 zCycle)))
