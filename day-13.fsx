//#region Intcode Computer (same as day 9)
type CpuState = Running | Halted | WaitingForInput
type Cpu = { State: CpuState; Mem: Map<int64, int64>; PC: int64; RelBase: int64; Input: int64 list; Output: int64 list }

let input = (System.IO.File.ReadAllText "day-13-input.txt").Split ","
let program = input |> Seq.indexed |> Seq.map (fun (x, y) -> int64 x, int64 y) |> Map
let initCpu = { State = Running; Mem = program; PC = 0L; RelBase = 0L; Input = []; Output = [] }

let rec run (cpu: Cpu): Cpu =
  let code = cpu.Mem.[cpu.PC] |> string |> fun x -> x.PadLeft(5, '0')

  let offset i =
    match code.[3 - i] with
    | '0' -> cpu.Mem.[cpu.PC + int64 i]
    | '1' -> cpu.PC + int64 i
    | '2' -> cpu.Mem.[cpu.PC + int64 i] + cpu.RelBase
    | _ -> raise <| System.Exception()

  let get i = cpu.Mem |> Map.tryFind (offset i) |> Option.defaultValue 0L
  let set i v = cpu.Mem |> Map.add (offset i) v

  match code.[3..4] with
  | "01" -> run { cpu with Mem = set 3 (get 1 + get 2); PC = cpu.PC + 4L }
  | "02" -> run { cpu with Mem = set 3 (get 1 * get 2); PC = cpu.PC + 4L }
  | "03" -> match cpu.Input with
            | [] -> { cpu with State = WaitingForInput }
            | x::y -> run { cpu with Mem = set 1 x; PC = cpu.PC + 2L; Input = y }
  | "04" -> run { cpu with Output = get 1 :: cpu.Output; PC = cpu.PC + 2L }
  | "05" -> run { cpu with PC = (if get 1 = 0L then cpu.PC + 3L else get 2) }
  | "06" -> run { cpu with PC = (if get 1 <> 0L then cpu.PC + 3L else get 2) }
  | "07" -> run { cpu with Mem = set 3 (if get 1 < get 2 then 1L else 0L); PC = cpu.PC + 4L }
  | "08" -> run { cpu with Mem = set 3 (if get 1 = get 2 then 1L else 0L); PC = cpu.PC + 4L }
  | "09" -> run { cpu with RelBase = cpu.RelBase + (get 1); PC = cpu.PC + 2L }
  | "99" -> { cpu with State = Halted }
  | _ -> raise <| System.Exception()
//#endregion

type Screen = { Score: int64; Tiles: Map<int64 * int64, int64> }

let processOutput cpu: Screen =
  let processCmd screen = function
    | [-1L; 0L; x] -> { screen with Score = x }
    | [x; y; tile] -> { screen with Tiles = screen.Tiles |> Map.add (x, y) tile }
    | _ -> raise <| System.Exception()
  cpu.Output
  |> List.rev
  |> List.chunkBySize 3
  |> List.fold processCmd { Score = 0L; Tiles = Map.empty }

let result = processOutput (run initCpu)
printfn "Part 1 %d" (result.Tiles |> Map.filter (fun _ x -> x = 2L) |> Map.count)

let rec play cpu =
  let cpu = run cpu
  let screen = processOutput cpu
  if screen.Tiles |> Map.exists (fun _ x -> x = 2L) then
    let (ball, _), _ = screen.Tiles |> Map.toSeq |> Seq.find (fun (_, x) -> x = 4L)
    let (paddle, _), _ = screen.Tiles |> Map.toSeq |> Seq.find (fun (_, x) -> x = 3L)
    let input = if ball < paddle then -1L elif ball > paddle then 1L else 0L
    play { cpu with Input = [input] }
  else
    screen.Score

printfn "Part 2: final score = %d" (play { initCpu with Mem = initCpu.Mem |> Map.add 0L 2L })
