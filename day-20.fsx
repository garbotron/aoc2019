
open System

let input = IO.File.ReadAllLines "day-20-input.txt"

let charMap =
  input
  |> Seq.indexed
  |> Seq.collect (fun (y, s) -> s |> Seq.indexed |> Seq.map (fun (x, c) -> ((x, y), c)))
  |> Map

type Node =
  { X: int; Y: int; Z: int; Label: string option; mutable Connections: Node list }
  member this.Coords = this.X, this.Y, this.Z

// Find the label for a given node based on the char map.
let findLabel (x, y) =
  let findChars x1 y1 x2 y2 =
    let l1 = charMap |> Map.tryFind (x1, y1)
    let l2 = charMap |> Map.tryFind (x2, y2)
    match l1, l2 with
    | Some c1, Some c2 when Char.IsLetter c1 && Char.IsLetter c2 -> Some (string c1 + string c2)
    | _ -> None

  [ findChars (x - 2) y (x - 1) y;
    findChars (x + 1) y (x + 2) y;
    findChars x (y - 2) x (y - 1);
    findChars x (y + 1) x (y + 2) ] |> List.choose id |> List.tryHead

// Create the initial set of nodes for each '.' in the character map (at Z=0 for part 1).
let bottomFloor =
  charMap
  |> Map.filter (fun _ x -> x = '.')
  |> Map.map (fun (x, y) _ -> { X = x; Y = y; Z = 0; Label = findLabel (x, y); Connections = [] })
  |> Map.toSeq
  |> Seq.map (fun (_, x) -> x.Coords, x)
  |> Map

let findNode label z map =
  map |> Map.filter (fun (_, _, z') n -> z' = z && n.Label = Some label) |> Map.toSeq |> Seq.head |> snd

let entrance nodes = nodes |> findNode "AA" 0
let exit nodes = nodes |> findNode "ZZ" 0

// Fill in the node connections based on adjacency.
let neighbors node z nodes =
  [ node.X - 1, node.Y; node.X + 1, node.Y; node.X, node.Y - 1; node.X, node.Y + 1]
  |> List.map (fun (x, y) -> nodes |> Map.tryFind (x, y, z))
  |> List.choose id

// Fill in additional magic portal connections according to labels.
let portals2d node nodes =
  if Option.isNone node.Label then
    []
  else
    nodes
    |> Map.toList
    |> List.map snd
    |> List.filter (fun x -> x.Label = node.Label && x.Coords <> node.Coords)

// Finally we can actually do the shortest path algorithm.
let rec explore (node: Node) cur cache =
  match !cache |> Map.tryFind node.Coords with
  | Some prev when prev <= cur -> ()
  | _ ->
    cache := !cache |> Map.add node.Coords cur
    node.Connections |> List.iter (fun n -> explore n (cur + 1) cache)

let nodes2d = bottomFloor |> Map.map (fun _ x -> x)
nodes2d |> Map.iter (fun _ x -> x.Connections <- List.concat [nodes2d |> neighbors x 0; nodes2d |> portals2d x] )
let map2d = ref Map.empty
explore (entrance nodes2d) 0 map2d
printfn "Part 1: %d" (!map2d |> Map.find (exit nodes2d).Coords)

// For part 2, create a 3D map based on our 2D map, where the inner portals go up (Z+1) and outer down (Z-1).
// Also make sure AA and ZZ apply only to the bottom floor (Z=0).
let allCoords = bottomFloor |> Map.toSeq |> Seq.map fst
let minX = allCoords |> Seq.map (fun (x, _, _) -> x) |> Seq.min
let maxX = allCoords |> Seq.map (fun (x, _, _) -> x) |> Seq.max
let minY = allCoords |> Seq.map (fun (_, y, _) -> y) |> Seq.min
let maxY = allCoords |> Seq.map (fun (_, y, _) -> y) |> Seq.max
let isOuter node = node.X = minX || node.X = maxX || node.Y = minY || node.Y = maxY

let create3dMap numFloors =
  let bottomNodes = bottomFloor |> Map.toSeq |> Seq.map snd |> Seq.filter (fun x -> Option.isSome x.Label)
  let innerLabels = bottomNodes |> Seq.filter (isOuter >> not) |> Seq.map (fun x -> x.Label, (x.X, x.Y)) |> Map
  let outerLabels = bottomNodes |> Seq.filter isOuter |> Seq.map (fun x -> x.Label, (x.X, x.Y)) |> Map

  let createFloor z =
    // Create the new floor based on the bottom floor, with nodes connected via adjacency.
    let floor = bottomFloor |> Map.toSeq |> Seq.map (fun ((x, y, _), n) -> (x, y, z), { n with Z = z }) |> Map
    floor |> Map.iter (fun _ x -> x.Connections <- floor |> neighbors x z)
    floor |> Map.toSeq

  let map = [ 0 .. numFloors - 1 ] |> Seq.collect createFloor |> Map

  // Now connect all of the inner portals on floor <N> to outer porals on floor <N+1>.
  for z in [ 0 .. numFloors - 2 ] do
    for lbl, (ix, iy) in innerLabels |> Map.toSeq do
      match outerLabels |> Map.tryFind lbl with
      | None -> ()
      | Some (ox, oy) ->
        let lower = map |> Map.find (ix, iy, z)
        let upper = map |> Map.find (ox, oy, z + 1)
        lower.Connections <- upper :: lower.Connections
        upper.Connections <- lower :: upper.Connections

  map

// Create the 3d map with a number of floors determined empirically to be a reasonable upper-bound.
let nodes3d = create3dMap 50
let map3d = ref Map.empty
explore (entrance nodes3d) 0 map3d
printfn "Part 2: %d" (!map3d |> Map.find (exit nodes3d).Coords)
