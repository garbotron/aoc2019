open System.Collections.Generic

type Formula = { Count: int64; Requires: (string * int64) list}

let parseFormula (str: string): string * Formula =
  let parseIngredient (str: string) = str.Split ' ' |> fun x -> x.[1], int64 x.[0]
  let parts = str.Split " => "
  let reqs = parts.[0].Split ", "
  let result, count = parseIngredient parts.[1]
  result, { Count = count; Requires = reqs |> Array.toList |> List.map parseIngredient }

let formulas = System.IO.File.ReadAllLines "day-14-input.txt" |> Seq.map (parseFormula) |> Map
let divup x y = (x + y - 1L) / y

// Creates the requested number of the requested chemical using ingredients on-hand, creating new ingredients using
// ore as needed. Returns the amount of ore used.
let rec calc (have: Dictionary<string, int64>) (chem: string, count: int64): int64 =
  if chem = "ORE" then
    count
  elif have.ContainsKey chem && have.[chem] >= count then
    have.[chem] <- have.[chem] - count
    0L
  else
    let numNeeded = count - (if have.ContainsKey chem then have.[chem] else 0L)
    let forumulaRuns = divup numNeeded formulas.[chem].Count
    let ingredients = formulas.[chem].Requires |> List.map (fun (x, y) -> x, y * forumulaRuns)
    have.[chem] <- (forumulaRuns * formulas.[chem].Count) - numNeeded
    ingredients |> List.fold (fun ore need -> ore + calc have need) 0L

printfn "Part 1: %d" (calc (new Dictionary<string, int64>()) ("FUEL", 1L))

// Binary search the amount of fuel that we should ask for in order to cost 1 trillion ore.
let rec findFuelCountForTrillionOre (min: int64) (max: int64): int64 =
  if min = max - 1L then min
  else
    let mid = (min + max) / 2L
    let result = calc (new Dictionary<string, int64>()) ("FUEL", mid)
    if result = 1000000000000L then mid
    elif result < 1000000000000L then findFuelCountForTrillionOre mid max
    else findFuelCountForTrillionOre min mid

printfn "Part 2: %d" (findFuelCountForTrillionOre 1L 1000000000000L)
