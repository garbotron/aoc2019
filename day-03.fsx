type Move = char * int
let parseMove (str: string) = str.[0], int str.[1..]
let parseMoves (str: string) = str.Split ',' |> Array.toList |> List.map parseMove

let input = System.IO.File.ReadAllLines "day-03-input.txt"
let wires = [input.[0]; input.[1]] |> List.map parseMoves

let rec path (x, y) = function
  | [] -> []
  | (dir, by) :: rest ->
    let iter =
      match dir with
      | 'L' -> fun v -> x - v, y
      | 'R' -> fun v -> x + v, y
      | 'U' -> fun v -> x, y + v
      | 'D' -> fun v -> x, y - v
      | _ -> raise <| System.Exception()
    let local = [ 1 .. by ] |> List.map iter
    local @ path (List.last local) rest

let paths = wires |> List.map (path (0, 0))
let intersections = paths |> List.map Set |> Set.intersectMany

let manhattan (x, y) = abs x + abs y
let stepsOnPath xy path = 1 + (path |> List.findIndex ((=) xy))
let steps xy = paths |> List.sumBy (stepsOnPath xy)

printfn "Part 1: %d" (intersections |> Set.map manhattan |> Set.minElement)
printfn "Part 2: %d" (intersections |> Set.map steps |> Set.minElement)
