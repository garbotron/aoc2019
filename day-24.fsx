let input =
  System.IO.File.ReadAllLines "day-24-input.txt"
  |> Seq.indexed
  |> Seq.map (fun (i, s) -> i, s |> Seq.indexed |> Seq.filter (fun (_, c) -> c = '#') |> Seq.map fst)
  |> Seq.collect (fun (y, xs) -> xs |> Seq.map (fun x -> x, y, 0))
  |> Set

let willHaveBug neighbors set coords =
  let neighborCount = neighbors coords |> Seq.filter (fun x -> set |> Set.contains x) |> Seq.length
  if set |> Set.contains coords then
    neighborCount = 1
  else
    neighborCount = 1 || neighborCount = 2

let nextGen neighbors set =
  set
  |> Set.union (set |> Set.toSeq |> Seq.collect neighbors |> Set)
  |> Set.filter (willHaveBug neighbors set)

let repeatUntilMatch neighbors set =
  let mutable set = set
  let mutable cache: Set<Set<int * int * int>> = Set.empty
  while not (cache |> Set.contains set) do
    cache <- cache |> Set.add set
    set <- nextGen neighbors set
  set

let biodiversity set =
  set |> Set.fold (fun n (x, y, _) -> n ||| (1 <<< (5 * y + x))) 0

let neighborsP1 (x, y, z) =
  let valid (x, y, _) = x >= 0 && x <= 4 && y >= 0 && y <= 4
  [(x - 1, y, z); (x + 1, y, z); (x, y - 1, z); (x, y + 1, z)] |> Seq.filter valid

printfn "Part 1: %d" (input |> repeatUntilMatch neighborsP1 |> biodiversity)

let neighborsP2 (x, y, z) =
  let left =  match x, y with
              | 0, _ -> [1, 2, z - 1]
              | 3, 2 -> [0..4] |> List.map (fun y -> 4, y, z + 1)
              | x, y -> [x - 1, y, z]

  let right = match x, y with
              | 4, _ -> [3, 2, z - 1]
              | 1, 2 -> [0..4] |> List.map (fun y -> 0, y, z + 1)
              | x, y -> [x + 1, y, z]

  let up =    match x, y with
              | _, 0 -> [2, 1, z - 1]
              | 2, 3 -> [0..4] |> List.map (fun x -> x, 4, z + 1)
              | x, y -> [x, y - 1, z]

  let down =  match x, y with
              | _, 4 -> [2, 3, z - 1]
              | 2, 1 -> [0..4] |> List.map (fun x -> x, 0, z + 1)
              | x, y -> [x, y + 1, z]

  left @ right @ up @ down

printfn "Part 2: %d" ([0..199] |> List.fold (fun set _ -> nextGen neighborsP2 set) input |> Set.count)
