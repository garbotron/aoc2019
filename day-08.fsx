let imgWidth = 25
let imgHeight = 6

let allCoords = seq { for y in { 0 .. imgHeight - 1 } do for x in { 0 .. imgWidth - 1 } do x, y }

let decode (str: string): int[,] list =
  let decodeLayer (str: char[]) =
    let ret = Array2D.create imgWidth imgHeight 0
    for x, y in allCoords do ret.[x, y] <- str.[(y * imgWidth) + x] |> string |> int
    ret
  str |> Seq.chunkBySize (imgWidth * imgHeight) |> Seq.toList |> List.map decodeLayer

let image = System.IO.File.ReadAllText "day-08-input.txt" |> fun x -> x.Trim() |> decode

let digitCount (digit: int) (layer: int[,]) =
  allCoords |> Seq.filter (fun (x, y) -> layer.[x, y] = digit) |> Seq.length

image
  |> List.minBy (digitCount 0)
  |> fun x -> (digitCount 1 x) * (digitCount 2 x)
  |> printfn "Part 1: %d"

printfn "Part 2:"

let getPixel x y = image |> List.map (fun a -> a.[x, y]) |> List.skipWhile ((=) 2) |> List.head |> ((=) 1)
for x, y in allCoords do
  printf "%c" (if getPixel x y then '0' else ' ')
  if x = imgWidth - 1 then printfn ""
