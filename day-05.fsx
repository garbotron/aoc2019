let input = System.IO.File.ReadAllText "day-05-input.txt"
let initialMemory = input.Split "," |> Seq.map int |> Seq.indexed |> Map

let rec run (mem: Map<int, int>) (pc: int) (input: int seq): int seq =
  let code = mem.[pc] |> string |> fun x -> x.PadLeft(5, '0')
  let getParam i = if code.[3 - i] = '1' then mem.[pc + i] else mem.[mem.[pc + i]]
  let setParam i v = mem |> Map.add mem.[pc + i] v
  match code.[3..4] with
  | "01" -> run (setParam 3 (getParam 1 + getParam 2)) (pc + 4) input
  | "02" -> run (setParam 3 (getParam 1 * getParam 2)) (pc + 4) input
  | "03" -> run (setParam 1 (Seq.head input)) (pc + 2) (Seq.tail input)
  | "04" -> seq { yield getParam 1; yield! run mem (pc + 2) input }
  | "05" -> if getParam 1 = 0 then run mem (pc + 3) input else run mem (getParam 2) input
  | "06" -> if getParam 1 <> 0 then run mem (pc + 3) input else run mem (getParam 2) input
  | "07" -> run (setParam 3 (if getParam 1 < getParam 2 then 1 else 0)) (pc + 4) input
  | "08" -> run (setParam 3 (if getParam 1 = getParam 2 then 1 else 0)) (pc + 4) input
  | "99" -> Seq.empty // halt (stop recursion)
  | _ -> raise <| System.Exception()

printfn "Part 1: %d" (run initialMemory 0 [1] |> Seq.last)
printfn "Part 2: %d" (run initialMemory 0 [5] |> Seq.exactlyOne)
