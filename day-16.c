#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static inline int elem(int *input, int input_len, int idx) {
    int to_add = 0;
    int to_sub = 0;

    for (int i = 0; i < input_len; i++) {
        int m = ((i + 1) % (4 * (idx + 1))) / (idx + 1);
        if (m == 1) {
            to_add += input[i]; // add to total
        } else if (m == 3) {
            to_sub += input[i]; // subtract from total
        } else {
            // don't interact with total
        }
    }

    return (to_add > to_sub ? to_add - to_sub : to_sub - to_add) % 10;
}

static inline void phase(int *input, int input_len, int *buf) {
    for (int i = 0; i < input_len; i++) {
        buf[i] = elem(input, input_len, i);
    }
    for (int i = 0; i < input_len; i++) {
        input[i] = buf[i];
    }
    static int phase_count = 0;
    printf("PHASE %d COMPLETE\n", phase_count++);
}

static inline void ftt(int phases, int *input, int input_len, int *buf) {
    for (int i = 0; i < phases; i++) {
        phase(input, input_len, buf);
    }
}

int main(void) {
    // File IO is horrible in C.
    FILE *fp = fopen("day-16-input.txt", "r");
    fseek(fp, 0L, SEEK_END);
    int file_size = ftell(fp);
    fseek(fp, 0L, SEEK_SET);
    int *input = malloc(sizeof(int) * file_size);
    int *buf = malloc(sizeof(int) * file_size);
    int input_len = 0;
    int ch = getc(fp);
    do {
        input[input_len] = ch - '0';
        input_len++;
        ch = getc(fp);
    } while (ch != '\n');
    fclose(fp);

    int ext_input_len = input_len * 2000;
    int *ext_input = malloc(sizeof(int) * ext_input_len);
    int *ext_buf = malloc(sizeof(int) * ext_input_len);
    for (int i = 0; i < ext_input_len; i++) {
        ext_input[i] = input[i % input_len];
    }

    printf("TEMP: %d\n", ext_input_len);

    // ftt(100, input, input_len, buf);
    // printf(
    //     "Part 1: %d%d%d%d%d%d%d%d\n",
    //     input[0], input[1], input[2], input[3], input[4], input[5], input[6], input[7]);

    int offset = (ext_input[0] * 1000000)
               + (ext_input[1] * 100000)
               + (ext_input[2] * 10000)
               + (ext_input[3] * 1000)
               + (ext_input[4] * 100)
               + (ext_input[5] * 10)
               + (ext_input[6]);

    ftt(100, ext_input, ext_input_len, buf);
    printf(
        "Part 2: %d%d%d%d%d%d%d%d\n",
        ext_input[offset + 0],
        ext_input[offset + 1],
        ext_input[offset + 2],
        ext_input[offset + 3],
        ext_input[offset + 4],
        ext_input[offset + 5],
        ext_input[offset + 6],
        ext_input[offset + 7]);
}
