type CpuState = Running | Halted | WaitingForInput
type Cpu = { State: CpuState; Mem: Map<int, int>; PC: int; Input: int list; Output: int list }

let program = (System.IO.File.ReadAllText "day-07-input.txt").Split "," |> Seq.map int |> Seq.indexed |> Map
let initCpu = { State = Running; Mem = program; PC = 0; Input = []; Output = [] }

// Intcode computer from part 5 (modified to take/return CPU state and stop on empty input buffer)
let rec run (cpu: Cpu): Cpu =
  let code = cpu.Mem.[cpu.PC] |> string |> fun x -> x.PadLeft(5, '0')
  let getParam i = if code.[3 - i] = '1' then cpu.Mem.[cpu.PC + i] else cpu.Mem.[cpu.Mem.[cpu.PC + i]]
  let setParam i v = cpu.Mem |> Map.add cpu.Mem.[cpu.PC + i] v
  match code.[3..4] with
  | "01" -> run { cpu with Mem = setParam 3 (getParam 1 + getParam 2); PC = cpu.PC + 4 }
  | "02" -> run { cpu with Mem = setParam 3 (getParam 1 * getParam 2); PC = cpu.PC + 4 }
  | "03" -> match cpu.Input with
            | [] -> { cpu with State = WaitingForInput }
            | x::y -> run { cpu with Mem = setParam 1 x; PC = cpu.PC + 2; Input = y }
  | "04" -> run { cpu with Output = getParam 1 :: cpu.Output; PC = cpu.PC + 2 }
  | "05" -> run { cpu with PC = (if getParam 1 = 0 then cpu.PC + 3 else getParam 2) }
  | "06" -> run { cpu with PC = (if getParam 1 <> 0 then cpu.PC + 3 else getParam 2) }
  | "07" -> run { cpu with Mem = setParam 3 (if getParam 1 < getParam 2 then 1 else 0); PC = cpu.PC + 4 }
  | "08" -> run { cpu with Mem = setParam 3 (if getParam 1 = getParam 2 then 1 else 0); PC = cpu.PC + 4 }
  | "99" -> { cpu with State = Halted }
  | _ -> raise <| System.Exception()

// https://dev.to/ducaale/computing-permutations-of-a-list-in-f-1n6k
let permute list =
  let rec inserts e = function
    | [] -> [[e]]
    | x::xs as list -> (e::list)::(inserts e xs |> List.map (fun xs' -> x::xs'))
  List.fold (fun accum x -> List.collect (inserts x) accum) [[]] list

let runAmpsInSeq phases =
  let runAmp input phase = run { initCpu with Input = [phase; input] } |> fun x -> List.head x.Output
  phases |> List.fold runAmp 0

printfn "Part 1: %d" (permute [0..4] |> List.map runAmpsInSeq |> List.max)

let runAmpsInRing phases =
  let vms = phases |> List.toArray |> Array.map (fun x -> run { initCpu with Input = [x]})
  let rec runVm idx v =
    if vms.[idx].State = WaitingForInput then
      vms.[idx] <- run { vms.[idx] with Input = [v] }
      runVm ((idx + 1) % vms.Length) (vms.[idx].Output |> List.head)
    else v
  runVm 0 0

printfn "Part 2: %d" (permute [5..9] |> List.map runAmpsInRing |> List.max)
