//#region Intcode Computer (same as day 9)
type CpuState = Running | Halted | WaitingForInput
type Cpu = { State: CpuState; Mem: Map<int64, int64>; PC: int64; RelBase: int64; Input: int64 list; Output: int64 list }

let input = (System.IO.File.ReadAllText "day-23-input.txt").Split ","
let program = input |> Seq.indexed |> Seq.map (fun (x, y) -> int64 x, int64 y) |> Map
let initCpu = { State = Running; Mem = program; PC = 0L; RelBase = 0L; Input = []; Output = [] }

let rec run (cpu: Cpu): Cpu =
  let code = cpu.Mem.[cpu.PC] |> string |> fun x -> x.PadLeft(5, '0')

  let offset i =
    match code.[3 - i] with
    | '0' -> cpu.Mem.[cpu.PC + int64 i]
    | '1' -> cpu.PC + int64 i
    | '2' -> cpu.Mem.[cpu.PC + int64 i] + cpu.RelBase
    | _ -> raise <| System.Exception()

  let get i = cpu.Mem |> Map.tryFind (offset i) |> Option.defaultValue 0L
  let set i v = cpu.Mem |> Map.add (offset i) v

  match code.[3..4] with
  | "01" -> run { cpu with Mem = set 3 (get 1 + get 2); PC = cpu.PC + 4L }
  | "02" -> run { cpu with Mem = set 3 (get 1 * get 2); PC = cpu.PC + 4L }
  | "03" -> match cpu.Input with
            | [] -> { cpu with State = WaitingForInput }
            | x::y -> run { cpu with Mem = set 1 x; PC = cpu.PC + 2L; Input = y }
  | "04" -> run { cpu with Output = get 1 :: cpu.Output; PC = cpu.PC + 2L }
  | "05" -> run { cpu with PC = (if get 1 = 0L then cpu.PC + 3L else get 2) }
  | "06" -> run { cpu with PC = (if get 1 <> 0L then cpu.PC + 3L else get 2) }
  | "07" -> run { cpu with Mem = set 3 (if get 1 < get 2 then 1L else 0L); PC = cpu.PC + 4L }
  | "08" -> run { cpu with Mem = set 3 (if get 1 = get 2 then 1L else 0L); PC = cpu.PC + 4L }
  | "09" -> run { cpu with RelBase = cpu.RelBase + (get 1); PC = cpu.PC + 2L }
  | "99" -> { cpu with State = Halted }
  | _ -> raise <| System.Exception()
//#endregion

type Packet =
  { Addr: int64; X: int64; Y: int64 }
  member this.Pack () = [this.X; this.Y]
  static member Unpack (x: int64 list) = { Addr = x.[2]; X = x.[1]; Y = x.[0] }

type NetworkState =
  { Cpus: Map<int64, Cpu>; NatPacket: Packet option; IdleCycles: int }

let runNetwork (state: NetworkState): NetworkState =
  // Add a -1L instruction for "empty queue" to all empty input lists, then run each CPU in turn.
  let cpus =
    state.Cpus
    |> Map.map (fun _ x -> if x.Input.IsEmpty then { x with Input = [-1L] } else x)
    |> Map.map (fun _ x -> run x)
  let output = cpus |> Map.toList |> List.map (fun (_, x) -> x.Output)
  let state = { state with Cpus = cpus |> Map.map (fun _ x -> { x with Output = [] }) }

  // Parse each CPU's output into packets.
  let packets = output |> List.collect (List.chunkBySize 3 >> List.map Packet.Unpack >> List.rev)
  if packets |> List.isEmpty then
    // The system is idle, record this.
    { state with IdleCycles = state.IdleCycles + 1 }
  else
    // Deliver each packet to its appropriate destination.
    let deliver state packet =
      if packet.Addr = 255L then
        { state with NatPacket = Some packet }
      else
        let dest = state.Cpus |> Map.find packet.Addr
        let cpus = state.Cpus |> Map.add packet.Addr { dest with Input = dest.Input @ packet.Pack() }
        { state with Cpus = cpus }
    packets |> List.fold deliver { state with IdleCycles = 0 }

let initCpus = List.init 50 (int64 >> fun x -> x, { initCpu with Input = [x] }) |> Map
let initState = { Cpus = initCpus; NatPacket = None; IdleCycles = 0 }

let runP1 state =
  let mutable state = state
  while state.NatPacket |> Option.isNone do
    state <- runNetwork state
  state.NatPacket |> Option.get |> fun x -> x.Y

printfn "Part 1: %d" (initState |> runP1)

let runP2 state =
  let idleCycleLimit = 5
  let mutable state = state
  let mutable ys = []
  while ys.Length < 2 || ys.[0] <> ys.[1] do
    while state.IdleCycles < idleCycleLimit do
      state <- runNetwork state
    let natPacket = state.NatPacket |> Option.get
    let dest = state.Cpus |> Map.find 0L
    let cpus = state.Cpus |> Map.add 0L { dest with Input = natPacket.Pack() }
    state <- { state with Cpus = cpus; IdleCycles = 0 }
    ys <- natPacket.Y :: ys
  ys.[0]

printfn "Part 2: %d" (initState |> runP2)
