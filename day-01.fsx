let input = System.IO.File.ReadAllLines "day-01-input.txt" |> Seq.map int

let wt x = x / 3 - 2

let rec totalWt x =
  let wt = wt x
  if wt <= 0 then 0 else wt + totalWt wt

printfn "Part 1: %d" (input |> Seq.sumBy wt)
printfn "Part 2: %d" (input |> Seq.sumBy totalWt)
