open System.Collections.Generic

//#region Intcode Computer (same as day 9)
type CpuState = Running | Halted | WaitingForInput
type Cpu = { State: CpuState; Mem: Map<int64, int64>; PC: int64; RelBase: int64; Input: int64 list; Output: int64 list }

let input = (System.IO.File.ReadAllText "day-15-input.txt").Split ","
let program = input |> Seq.indexed |> Seq.map (fun (x, y) -> int64 x, int64 y) |> Map
let initCpu = { State = Running; Mem = program; PC = 0L; RelBase = 0L; Input = []; Output = [] }

let rec run (cpu: Cpu): Cpu =
  let code = cpu.Mem.[cpu.PC] |> string |> fun x -> x.PadLeft(5, '0')

  let offset i =
    match code.[3 - i] with
    | '0' -> cpu.Mem.[cpu.PC + int64 i]
    | '1' -> cpu.PC + int64 i
    | '2' -> cpu.Mem.[cpu.PC + int64 i] + cpu.RelBase
    | _ -> raise <| System.Exception()

  let get i = cpu.Mem |> Map.tryFind (offset i) |> Option.defaultValue 0L
  let set i v = cpu.Mem |> Map.add (offset i) v

  match code.[3..4] with
  | "01" -> run { cpu with Mem = set 3 (get 1 + get 2); PC = cpu.PC + 4L }
  | "02" -> run { cpu with Mem = set 3 (get 1 * get 2); PC = cpu.PC + 4L }
  | "03" -> match cpu.Input with
            | [] -> { cpu with State = WaitingForInput }
            | x::y -> run { cpu with Mem = set 1 x; PC = cpu.PC + 2L; Input = y }
  | "04" -> run { cpu with Output = get 1 :: cpu.Output; PC = cpu.PC + 2L }
  | "05" -> run { cpu with PC = (if get 1 = 0L then cpu.PC + 3L else get 2) }
  | "06" -> run { cpu with PC = (if get 1 <> 0L then cpu.PC + 3L else get 2) }
  | "07" -> run { cpu with Mem = set 3 (if get 1 < get 2 then 1L else 0L); PC = cpu.PC + 4L }
  | "08" -> run { cpu with Mem = set 3 (if get 1 = get 2 then 1L else 0L); PC = cpu.PC + 4L }
  | "09" -> run { cpu with RelBase = cpu.RelBase + (get 1); PC = cpu.PC + 2L }
  | "99" -> { cpu with State = Halted }
  | _ -> raise <| System.Exception()
//#endregion

type TileType = Wall | Open | Oxygen
type TileState = { Type: TileType; FewestSteps: int }

let outputToType = function
  | [0L] -> Wall
  | [1L] | [] -> Open
  | [2L] -> Oxygen
  | _ -> raise <| System.Exception()

let rec exploreAll (cpu: Cpu) (step: int) (x: int) (y: int) (map: Dictionary<int * int, TileState>) =
  let tile = outputToType cpu.Output
  if tile = Wall then
    () // stop at walls
  elif map.ContainsKey (x, y) && map.[x, y].FewestSteps <= step then
    () // there is already a shorter path to get here
  else
    let explore dir dx dy =
      let afterMove = run { cpu with Input = [dir]; Output = [] }
      exploreAll afterMove (step + 1) (x + dx) (y + dy) map
    map.[(x, y)] <- { Type = tile; FewestSteps = step }
    explore 1L 0 -1
    explore 2L 0 1
    explore 3L -1 0
    explore 4L 1 0

let tiles = new Dictionary<int * int, TileState>()
exploreAll (run initCpu) 0 0 0 tiles
let oxygenTile = tiles |> Seq.find (fun x -> x.Value.Type = Oxygen)

printfn "Part 1: %d" oxygenTile.Value.FewestSteps

let rec turnsToFill (oxygen: Set<int * int>) (empty: Set<int * int>): int =
  if Set.isEmpty empty then 0
  else
    let neighbors x y = [x - 1, y; x + 1, y; x, y - 1; x, y + 1]
    let fillNextTurn (x, y) = neighbors x y |> List.exists (fun x -> oxygen |> Set.contains x)
    let toFill = empty |> Set.filter fillNextTurn
    1 + turnsToFill (Set.union oxygen toFill) (Set.difference empty toFill)

let oxygen = tiles |> Seq.filter (fun x -> x.Value.Type = Oxygen) |> Seq.map (fun x -> x.Key) |> Set
let empty = tiles |> Seq.filter (fun x -> x.Value.Type = Open) |> Seq.map (fun x -> x.Key) |> Set
printfn "Part 2: %d" (turnsToFill oxygen empty)
