open System.IO
open System.Text

//#region Intcode Computer (same as day 9)
type CpuState = Running | Halted | WaitingForInput
type Cpu = { State: CpuState; Mem: Map<int64, int64>; PC: int64; RelBase: int64; Input: int64 list; Output: int64 list }

let input = (System.IO.File.ReadAllText "day-25-input.txt").Split ","
let program = input |> Seq.indexed |> Seq.map (fun (x, y) -> int64 x, int64 y) |> Map
let initCpu = { State = Running; Mem = program; PC = 0L; RelBase = 0L; Input = []; Output = [] }

let rec run (cpu: Cpu): Cpu =
  let code = cpu.Mem.[cpu.PC] |> string |> fun x -> x.PadLeft(5, '0')

  let offset i =
    match code.[3 - i] with
    | '0' -> cpu.Mem.[cpu.PC + int64 i]
    | '1' -> cpu.PC + int64 i
    | '2' -> cpu.Mem.[cpu.PC + int64 i] + cpu.RelBase
    | _ -> raise <| System.Exception()

  let get i = cpu.Mem |> Map.tryFind (offset i) |> Option.defaultValue 0L
  let set i v = cpu.Mem |> Map.add (offset i) v

  match code.[3..4] with
  | "01" -> run { cpu with Mem = set 3 (get 1 + get 2); PC = cpu.PC + 4L }
  | "02" -> run { cpu with Mem = set 3 (get 1 * get 2); PC = cpu.PC + 4L }
  | "03" -> match cpu.Input with
            | [] -> { cpu with State = WaitingForInput }
            | x::y -> run { cpu with Mem = set 1 x; PC = cpu.PC + 2L; Input = y }
  | "04" -> run { cpu with Output = get 1 :: cpu.Output; PC = cpu.PC + 2L }
  | "05" -> run { cpu with PC = (if get 1 = 0L then cpu.PC + 3L else get 2) }
  | "06" -> run { cpu with PC = (if get 1 <> 0L then cpu.PC + 3L else get 2) }
  | "07" -> run { cpu with Mem = set 3 (if get 1 < get 2 then 1L else 0L); PC = cpu.PC + 4L }
  | "08" -> run { cpu with Mem = set 3 (if get 1 = get 2 then 1L else 0L); PC = cpu.PC + 4L }
  | "09" -> run { cpu with RelBase = cpu.RelBase + (get 1); PC = cpu.PC + 2L }
  | "99" -> { cpu with State = Halted }
  | _ -> raise <| System.Exception()
//#endregion

let save cpu filename =
  use stream = File.OpenWrite(filename)
  use writer = new BinaryWriter(stream)
  writer.Write cpu.PC
  writer.Write cpu.RelBase
  for addr, v in cpu.Mem |> Map.toSeq do
    writer.Write addr
    writer.Write v

let load filename =
  use stream = File.OpenRead(filename)
  use reader = new BinaryReader(stream)
  let pc = reader.ReadInt64()
  let relBase = reader.ReadInt64()
  let mutable mem: Map<int64, int64> = Map.empty
  while stream.Position < stream.Length do
    mem <- mem |> Map.add (reader.ReadInt64()) (reader.ReadInt64())
  { initCpu with PC = pc; RelBase = relBase; Mem = mem }

let encodeCommand (str: string) =
  str |> Encoding.ASCII.GetBytes |> Array.toList |> List.map int64 |> fun x -> x @ [10L]

let decodeOutput (output: int64 list) =
  output |> List.rev |> List.map byte |> List.toArray |> Encoding.ASCII.GetString

let westBlocked cpu =
  let cpuAfterWest = { cpu with Input = encodeCommand "west" } |> run
  let output = cpuAfterWest.Output |> decodeOutput
  output.Contains "than the detected value"

let allItems = ["space law space brochure"; "mouse"; "astrolabe"; "mug"; "sand"; "manifold"; "monolith"; "wreath"]

let dropItems items cpu =
  items |> List.fold (fun cpu item -> { cpu with Input = encodeCommand ("drop " + item) } |> run) cpu

let takeItems items cpu =
  items |> List.fold (fun cpu item -> { cpu with Input = encodeCommand ("take " + item) } |> run) cpu

let isItemSetCorrect items cpu =
  cpu |> dropItems allItems |> takeItems items |> westBlocked |> not

let findCorrectItems cpu =
  let bmpToItemSet bmp =
    allItems |> List.indexed |> List.filter (fun (i, _) -> (bmp &&& (1 <<< i)) <> 0) |> List.map snd
  let max = 1 <<< allItems.Length
  [0 .. max - 1] |> Seq.map bmpToItemSet |> Seq.find (fun x -> isItemSetCorrect x cpu)

let bruteForce cpu =
  printfn "Correct item set: %A" (findCorrectItems cpu)

let rec inputCommand cpu =
  match System.Console.ReadLine() with
  | "save" -> save cpu "save.bin"; inputCommand cpu
  | "load" -> load "save.bin" |> inputCommand
  | "brute" -> bruteForce cpu ; inputCommand cpu
  | x -> cpu, x

let mutable cpu = initCpu
while true do
  cpu <- cpu |> run
  printf "%s" (cpu.Output |> decodeOutput)
  let cpu', command = inputCommand cpu
  cpu <- { cpu' with Output = []; Input = encodeCommand command }

(*
NOTES

ITEMS (that don't kill you)

space law space brochure
mouse
astrolabe
mug
sand
manifold
monolith
wreath

Save file created with all items in hand, with the pressure-sensitive floor to the west.

*)
