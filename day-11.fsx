//#region Intcode Computer (same as day 9)
type CpuState = Running | Halted | WaitingForInput
type Cpu = { State: CpuState; Mem: Map<int64, int64>; PC: int64; RelBase: int64; Input: int64 list; Output: int64 list }

let input = (System.IO.File.ReadAllText "day-11-input.txt").Split ","
let program = input |> Seq.indexed |> Seq.map (fun (x, y) -> int64 x, int64 y) |> Map
let initCpu = { State = Running; Mem = program; PC = 0L; RelBase = 0L; Input = []; Output = [] }

let rec run (cpu: Cpu): Cpu =
  let code = cpu.Mem.[cpu.PC] |> string |> fun x -> x.PadLeft(5, '0')

  let offset i =
    match code.[3 - i] with
    | '0' -> cpu.Mem.[cpu.PC + int64 i]
    | '1' -> cpu.PC + int64 i
    | '2' -> cpu.Mem.[cpu.PC + int64 i] + cpu.RelBase
    | _ -> raise <| System.Exception()

  let get i = cpu.Mem |> Map.tryFind (offset i) |> Option.defaultValue 0L
  let set i v = cpu.Mem |> Map.add (offset i) v

  match code.[3..4] with
  | "01" -> run { cpu with Mem = set 3 (get 1 + get 2); PC = cpu.PC + 4L }
  | "02" -> run { cpu with Mem = set 3 (get 1 * get 2); PC = cpu.PC + 4L }
  | "03" -> match cpu.Input with
            | [] -> { cpu with State = WaitingForInput }
            | x::y -> run { cpu with Mem = set 1 x; PC = cpu.PC + 2L; Input = y }
  | "04" -> run { cpu with Output = get 1 :: cpu.Output; PC = cpu.PC + 2L }
  | "05" -> run { cpu with PC = (if get 1 = 0L then cpu.PC + 3L else get 2) }
  | "06" -> run { cpu with PC = (if get 1 <> 0L then cpu.PC + 3L else get 2) }
  | "07" -> run { cpu with Mem = set 3 (if get 1 < get 2 then 1L else 0L); PC = cpu.PC + 4L }
  | "08" -> run { cpu with Mem = set 3 (if get 1 = get 2 then 1L else 0L); PC = cpu.PC + 4L }
  | "09" -> run { cpu with RelBase = cpu.RelBase + (get 1); PC = cpu.PC + 2L }
  | "99" -> { cpu with State = Halted }
  | _ -> raise <| System.Exception()
//#endregion

type Dir =  Up | Right | Down | Left
type Robot = { Cpu: Cpu; Dir: Dir; Pos: int * int; Tiles: Map<int * int, int64> }

let next = function | Up -> Right | Right -> Down | Down -> Left | Left -> Up
let prev = function | Up -> Left | Right -> Up | Down -> Right | Left -> Down
let travel (x, y) = function | Up -> x, y - 1 | Right -> x + 1, y | Down -> x, y + 1 | Left -> x - 1, y

let rec robot (rob: Robot): Robot =
  if rob.Cpu.Output.Length >= 2 then
    let paint = rob.Cpu.Output.[1]
    let move = rob.Cpu.Output.[0]
    let tiles = rob.Tiles |> Map.add rob.Pos paint
    let dir = if move = 0L then prev rob.Dir else next rob.Dir
    let pos = travel rob.Pos dir
    robot { Cpu = { rob.Cpu with Output = rob.Cpu.Output.[2..] }; Dir = dir; Pos = pos; Tiles = tiles }
  elif rob.Cpu.State = WaitingForInput then
    let input = rob.Tiles |> Map.tryFind rob.Pos |> Option.defaultValue 0L
    robot { rob with Cpu = run { rob.Cpu with Input = [input] } }
  elif rob.Cpu.State = Halted then
    rob // done
  else
    raise <| System.Exception()

let p1 = robot { Cpu = run initCpu; Dir = Up; Pos = 0, 0; Tiles = Map.empty }
printfn "Part 1: %d" (p1.Tiles |> Map.count)

printfn "Part 2:"
let p2 = robot { Cpu = run initCpu; Dir = Up; Pos = 0, 0; Tiles = Map [(0, 0), 1L] }
let xs = p2.Tiles |> Map.toSeq |> Seq.map (fst >> fst)
let ys = p2.Tiles |> Map.toSeq |> Seq.map (fst >> snd)
for y in { Seq.min ys .. Seq.max ys } do
  for x in { Seq.min xs .. Seq.max xs } do
    printf (if p2.Tiles |> Map.tryFind (x, y) |> Option.defaultValue 0L = 1L then "#" else " ")
  printfn ""
