let input = System.IO.File.ReadAllText "day-02-input.txt"
let program = input.Split "," |> Seq.map int |> Seq.indexed |> Map

let rec run (pc: int) (prog: Map<int, int>) =
  let get offset = prog.[prog.[pc + offset]]
  let set offset v = prog |> Map.add prog.[pc + offset] v
  match prog.[pc] with
  | 1 -> run (pc + 4) (set 3 (get 1 + get 2))
  | 2 -> run (pc + 4) (set 3 (get 1 * get 2))
  | 99 -> prog
  | _ -> raise <| System.Exception()

let runInput noun verb =
  program
  |> Map.add 1 noun
  |> Map.add 2 verb
  |> run 0
  |> Map.find 0

let findInputs output: int * int =
  seq { for i in { 0 .. 99 } do for j in { 0 .. 99 } do i, j }
  |> Seq.find (fun (x, y) -> runInput x y = output)

printfn "Part 1: %d" (runInput 12 2)
printfn "Part 2: %d" (findInputs 19690720 |> fun (x, y) -> x * 100 + y)
