//#region Intcode Computer (same as day 9)
type CpuState = Running | Halted | WaitingForInput
type Cpu = { State: CpuState; Mem: Map<int64, int64>; PC: int64; RelBase: int64; Input: int64 list; Output: int64 list }

let input = (System.IO.File.ReadAllText "day-17-input.txt").Split ","
let program = input |> Seq.indexed |> Seq.map (fun (x, y) -> int64 x, int64 y) |> Map
let initCpu = { State = Running; Mem = program; PC = 0L; RelBase = 0L; Input = []; Output = [] }

let rec run (cpu: Cpu): Cpu =
  let code = cpu.Mem.[cpu.PC] |> string |> fun x -> x.PadLeft(5, '0')

  let offset i =
    match code.[3 - i] with
    | '0' -> cpu.Mem.[cpu.PC + int64 i]
    | '1' -> cpu.PC + int64 i
    | '2' -> cpu.Mem.[cpu.PC + int64 i] + cpu.RelBase
    | _ -> raise <| System.Exception()

  let get i = cpu.Mem |> Map.tryFind (offset i) |> Option.defaultValue 0L
  let set i v = cpu.Mem |> Map.add (offset i) v

  match code.[3..4] with
  | "01" -> run { cpu with Mem = set 3 (get 1 + get 2); PC = cpu.PC + 4L }
  | "02" -> run { cpu with Mem = set 3 (get 1 * get 2); PC = cpu.PC + 4L }
  | "03" -> match cpu.Input with
            | [] -> { cpu with State = WaitingForInput }
            | x::y -> run { cpu with Mem = set 1 x; PC = cpu.PC + 2L; Input = y }
  | "04" -> run { cpu with Output = get 1 :: cpu.Output; PC = cpu.PC + 2L }
  | "05" -> run { cpu with PC = (if get 1 = 0L then cpu.PC + 3L else get 2) }
  | "06" -> run { cpu with PC = (if get 1 <> 0L then cpu.PC + 3L else get 2) }
  | "07" -> run { cpu with Mem = set 3 (if get 1 < get 2 then 1L else 0L); PC = cpu.PC + 4L }
  | "08" -> run { cpu with Mem = set 3 (if get 1 = get 2 then 1L else 0L); PC = cpu.PC + 4L }
  | "09" -> run { cpu with RelBase = cpu.RelBase + (get 1); PC = cpu.PC + 2L }
  | "99" -> { cpu with State = Halted }
  | _ -> raise <| System.Exception()
//#endregion

type Facing = Left | Right | Up | Down
type Tile = Open | Scaffold | Robot of Facing | Dead
type Step = TurnLeft | TurnRight | Forward of int
type RobotState = { X: int; Y: int; Facing: Facing }

let parseMap (cpu: Cpu): Map<int * int, Tile> =
  let rec parse output map x y =
    match output with
    | [] -> map
    | 10L  :: tail -> parse tail map 0 (y + 1)
    | 46L  :: tail -> parse tail (map |> Map.add (x, y) Open) (x + 1) y
    | 35L  :: tail -> parse tail (map |> Map.add (x, y) Scaffold) (x + 1) y
    | 94L  :: tail -> parse tail (map |> Map.add (x, y) (Robot Up)) (x + 1) y
    | 118L :: tail -> parse tail (map |> Map.add (x, y) (Robot Down)) (x + 1) y
    | 60L  :: tail -> parse tail (map |> Map.add (x, y) (Robot Left)) (x + 1) y
    | 62L  :: tail -> parse tail (map |> Map.add (x, y) (Robot Right)) (x + 1) y
    | 88L  :: tail -> parse tail (map |> Map.add (x, y) Dead) (x + 1) y
    | _ -> raise <| System.Exception()
  parse (cpu.Output |> List.rev) Map.empty 0 0

let findIntersections (map: Map<int * int, Tile>): (int * int) list =
  let isIntersection (x, y) =
    let cross = [x, y; x - 1, y; x + 1, y; x, y - 1; x, y + 1]
    cross |> List.forall (fun x -> map |> Map.tryFind x = Some Scaffold)
  map |> Map.toSeq |> Seq.map fst |> Seq.filter isIntersection |> Seq.toList

let map = run initCpu |> parseMap
let intersections = map |> findIntersections

printfn "Part 1: %d" (intersections |> List.sumBy (fun (x, y) -> x * y))

let takeStep step state =
  match state.Facing, step with
  | Left,  TurnLeft | Right, TurnRight -> { state with Facing = Down }
  | Right, TurnLeft | Left,  TurnRight -> { state with Facing = Up }
  | Up,    TurnLeft | Down,  TurnRight -> { state with Facing = Left }
  | Down,  TurnLeft | Up,    TurnRight -> { state with Facing = Right }
  | Left,  Forward x -> { state with X = state.X - x }
  | Right, Forward x -> { state with X = state.X + x }
  | Up,    Forward y -> { state with Y = state.Y - y }
  | Down,  Forward y -> { state with Y = state.Y + y }

let computeFullPath (map: Map<int * int, Tile>): Step list =
  let tileFacing = function | x, Robot y -> Some (x, y) | _ -> None
  let (x, y), facing = map |> Map.toSeq |> Seq.choose tileFacing |> Seq.head
  let fwdIsValid state x = state |> takeStep (Forward x) |> fun s -> map |> Map.tryFind (s.X, s.Y) = Some Scaffold
  let nextStep state =
    match ({ 1 .. System.Int32.MaxValue } |> Seq.takeWhile (fwdIsValid state) |> Seq.tryLast) with
    | Some x -> Some (Forward x)
    | None when fwdIsValid (state |> takeStep TurnLeft) 1 -> Some TurnLeft
    | None when fwdIsValid (state |> takeStep TurnRight) 1 -> Some TurnRight
    | None -> None
  let stepper state =
    match nextStep state with
    | Some x -> Some (x, state |> takeStep x)
    | None -> None
  List.unfold stepper { X = x; Y = y; Facing = facing }

let substrMatch (full: 'a list) (part: 'a list) = full.Length >= part.Length && full |> List.take part.Length = part

// Skip list: set of integers (3 in our case) that determines how functions A, B and C are extracted.
// The numbers represent how long the substring should be. Say the step list is [x, y, z]. Function A will be
// the first x steps of the step list. Function B will be the following y steps, unless another copy of function
// A is encountered first. Same for C. Permuting these numbers lets us search for the solution.
let rec extractFuncs (path: Step list) (funcs: Step list list) (skips: int list): Step list list option =
  let rec advance path =
    match funcs |> List.tryFind (substrMatch path) with
    | None -> path // no functions matched
    | Some x -> x |> (fun x -> path |> List.skip x.Length) |> advance
  let path = advance path
  match skips with
  | _ when List.isEmpty path -> Some funcs
  | [] -> None
  | skip :: _ when path.Length < skip -> None
  | skip :: otherSkips -> extractFuncs (path |> List.skip skip) ((path |> List.take skip) :: funcs) otherSkips

// Transform an input path along with associated functions into a list of function references (by index).
let rec getFuncRefs (path: Step list) (funcs: Step list list): int list =
  if List.isEmpty path then []
  else
    let i, s = funcs |> List.indexed |> List.find (snd >> substrMatch path)
    i :: getFuncRefs (path |> List.skip s.Length) funcs

let path = map |> computeFullPath
let allPossibleSkips = seq {
  for i in { 1 .. 20 } do
    for j in { 1 .. 20 } do
      for k in { 1 .. 20 } do [i; j; k] }
let funcs = allPossibleSkips |> Seq.choose (extractFuncs path []) |> Seq.minBy List.length
let funcRefs = getFuncRefs path funcs

// Now that we have everthing calculated, run the actual program.
let join strs = strs |> List.reduce (fun x y -> x + "," + y)
let command str = str + "\n" |> Seq.toList |> List.map int64
let mainRoutine = funcRefs |> List.map (char >> ((+) 'A') >> string) |> join
let stepStr = function | TurnLeft -> "L" | TurnRight -> "R" | Forward x -> string x
let funcRoutine steps = steps |> List.map stepStr |> join

let cpu =
  run { initCpu with Mem = initCpu.Mem |> Map.add 0L 2L }
  |> fun x -> run { x with Input = command mainRoutine }
  |> fun x -> run { x with Input = command (funcRoutine funcs.[0]) }
  |> fun x -> run { x with Input = command (funcRoutine funcs.[1]) }
  |> fun x -> run { x with Input = command (funcRoutine funcs.[2]) }
  |> fun x -> run { x with Input = command "n"; Output = [] } // no video feed

printfn "Part 2: %d" cpu.Output.[0]
